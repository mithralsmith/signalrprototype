﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Security.Principal;
using System.Text;
using ESI.Interfaces;
using ESI.Services;

namespace ESI.Context
{
    public class WorkstationContext: UserContext, IWorkstationContext
    {
        /// <summary>
        /// Create  workstation context with a special identity
        /// </summary>
        /// <param name="identity">
        /// A principal identity such as WindowsIdentity or custom principal identity.
        /// </param>
        public WorkstationContext(IIdentity identity, IAuthorizationService authService, IPermissionsService permissionService)
        {
            _Name = identity.Name;
            _isAuthenticated = identity.IsAuthenticated;
            _authorizationService = authService;
            _permissionsService = permissionService;
        }
        /// <summary>
        /// Create a workstation context using the currently logged in windows user
        /// </summary>
        public WorkstationContext():this(WindowsIdentity.GetCurrent(), DefaultFactory.AuthorizationService, DefaultFactory.PermissionService)
        {

        }

        /// <summary>
        /// Get only is this workstation authorized to use the Alerts module.
        /// </summary>
        public bool IsAuthorizedAlerts
        {
            get
            {
               return HasPermission(PermissionAction.StartAlertModule);
            }
        }
    }
}