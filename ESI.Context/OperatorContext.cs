﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using ESI.Interfaces;
using ESI.Services;

namespace ESI.Context
{

    public class OperatorContext:UserContext
    {
        /// <summary>
        /// Create  workstation context with a special identity
        /// </summary>
        /// <param name="identity">
        /// A principal identity such as WindowsIdentity or custom principal identity.
        /// </param>
        public OperatorContext(IIdentity identity, IAuthorizationService authService, IPermissionsService permissionService)
        {
            _Name = identity.Name;
            _isAuthenticated = identity.IsAuthenticated;
            _authorizationService = authService;
            _permissionsService = permissionService;
        }

        /// <summary>
        /// Create a workstation context using the currently logged in windows user
        /// </summary>
        public OperatorContext():this(WindowsIdentity.GetCurrent(), DefaultFactory.AuthorizationService, DefaultFactory.PermissionService)
        {

        }
    }
}
