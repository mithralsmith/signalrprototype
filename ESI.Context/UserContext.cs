﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using ESI.Interfaces;
using ESI.Services;

namespace ESI.Context
{
    public class UserContext: IUserContext, IPopulatePermissions
    {
        protected string _Name;
        protected bool _isAuthenticated;
        protected bool? _isAuthorized;
        protected bool _isAuthorizedAlerts;
        protected IEnumerable<IPermission> _permissionList;
        protected IAuthorizationService _authorizationService;
        protected IPermissionsService _permissionsService;
        protected IDictionary<PermissionAction, IPermission> _permissionMap;


        /// <summary>
        /// Create  workstation context with a special identity
        /// </summary>
        /// <param name="identity">
        /// A principal identity such as WindowsIdentity or custom principal identity.
        /// </param>
        public UserContext(IIdentity identity, IAuthorizationService authService, IPermissionsService permissionService)
        {
            _Name = identity.Name;
            _isAuthenticated = identity.IsAuthenticated;
            _authorizationService = authService;
            _permissionsService = permissionService;
        }
        /// <summary>
        /// Create a workstation context using the currently logged in windows user
        /// </summary>
        public UserContext():this(WindowsIdentity.GetCurrent(), DefaultFactory.AuthorizationService, DefaultFactory.PermissionService)
        {

        }


        #region Implementation of IWorkstationContext

        /// <summary>
        /// Get only the name of the workstation.
        /// </summary>
        public string Name
        {
            get { return _Name; }
        }

        /// <summary>
        /// Get only is this workstation authenticated. 
        /// </summary>
        public bool IsAuthenticated
        {
            get { return _isAuthenticated; }
        }

        /// <summary>
        /// Get only is this workstation authorized to use any modules.
        /// </summary>
        public bool IsAuthorized
        {
            get
            {
                if (!_isAuthorized.HasValue)
                {
                    Authorize();
                }
                return _isAuthorized.Value;
            }
            private set { _isAuthorized = value; }
        }



        /// <summary>
        /// Verifies that the context is authorized and sets the properties
        /// accordingly.
        /// </summary>
        /// <returns>Returns true if the context is authorized for this application.</returns>
        public bool Authorize()
        {
            IsAuthorized = _authorizationService.Authorize(this);
            _permissionsService.PopulatePermissions(this);

            return IsAuthorized;
        }

        /// <summary>
        /// Determines if the user has the specified permission.
        /// </summary>
        /// <param name="action">The name of the permission.</param>
        /// <returns>True if the user in this context has the specified permission.</returns>
        public bool HasPermission(PermissionAction action)
        {
            return _permissionMap.ContainsKey(action);
        }
        /// <summary>
        /// get only a list of permissions;
        /// </summary>
        public IEnumerable<IPermission> PermissionList
        {
            get { return _permissionList; }
        }


        #endregion
        #region Implementation of IPopulatePermissions

        public void PopulatePermissions(IEnumerable<IPermission> permissionList)
        {
            //a concurrent dictionary is not needed here, but I used it to demonstrate
            //  how to use a concurrent dictionary.
            _permissionMap = new ConcurrentDictionary<PermissionAction, IPermission>();
            foreach (IPermission permission in permissionList)
            {
                _permissionMap.Add(permission.Action, permission);
                //Note: we could do the permission map by name but I chose to use
                // an action enumeration so that we don't have to use strings that
                // could be fat fingered.
                // Strings, though are far more flexible when it comes to adding a new
                // action/permission.
            }
            _permissionList = _permissionMap.Values;

        }

        #endregion

        #region Overrides of Object

        /// <summary>Returns a string that represents the current object.</summary>
        /// <returns>A string that represents the current object.</returns>
        public override string ToString()
        {
            string authenticated = IsAuthenticated ? "is" : "is not";
            string authorized = IsAuthorized ? "is" : "is not";
            StringBuilder permissions = new StringBuilder();
            foreach (var permission in PermissionList)
            {
                permissions.AppendLine($"   {permission}");
            }

            return $"{Name}, {authenticated} Authenticated, {authorized} Authorized, -> \n{permissions} ";
        }




        #endregion
    }
}
