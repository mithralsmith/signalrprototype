﻿namespace WinFormsClient
{
    partial class WinFormsClient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ButtonSend = new System.Windows.Forms.Button();
            this.TextBoxMessage = new System.Windows.Forms.TextBox();
            this.RichTextBoxConsole = new System.Windows.Forms.RichTextBox();
            this.ChatPanel = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonAcknowledgeAlarm = new System.Windows.Forms.Button();
            this.buttonMaskSensor = new System.Windows.Forms.Button();
            this.textBoxUserName = new System.Windows.Forms.TextBox();
            this.buttonChangeUser = new System.Windows.Forms.Button();
            this.TextBoxPassword = new System.Windows.Forms.TextBox();
            this.SignInPanel = new System.Windows.Forms.Panel();
            this.SignInMessagesTextBox = new System.Windows.Forms.TextBox();
            this.DomainTextBox = new System.Windows.Forms.TextBox();
            this.SwitchUserBtn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.URITextBox = new System.Windows.Forms.TextBox();
            this.labelWorkstationName = new System.Windows.Forms.Label();
            this.PasswordTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ConnectBtn = new System.Windows.Forms.Button();
            this.UserNameTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.StatusText = new System.Windows.Forms.Label();
            this.ChatPanel.SuspendLayout();
            this.SignInPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ButtonSend
            // 
            this.ButtonSend.Enabled = false;
            this.ButtonSend.Location = new System.Drawing.Point(627, 51);
            this.ButtonSend.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ButtonSend.Name = "ButtonSend";
            this.ButtonSend.Size = new System.Drawing.Size(112, 35);
            this.ButtonSend.TabIndex = 1;
            this.ButtonSend.Text = "Send";
            this.ButtonSend.UseVisualStyleBackColor = true;
            this.ButtonSend.Click += new System.EventHandler(this.ButtonSend_Click);
            // 
            // TextBoxMessage
            // 
            this.TextBoxMessage.Location = new System.Drawing.Point(12, 60);
            this.TextBoxMessage.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.TextBoxMessage.Name = "TextBoxMessage";
            this.TextBoxMessage.Size = new System.Drawing.Size(580, 26);
            this.TextBoxMessage.TabIndex = 2;
            // 
            // RichTextBoxConsole
            // 
            this.RichTextBoxConsole.Location = new System.Drawing.Point(12, 179);
            this.RichTextBoxConsole.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.RichTextBoxConsole.Name = "RichTextBoxConsole";
            this.RichTextBoxConsole.Size = new System.Drawing.Size(720, 586);
            this.RichTextBoxConsole.TabIndex = 3;
            this.RichTextBoxConsole.Text = "";
            // 
            // ChatPanel
            // 
            this.ChatPanel.Controls.Add(this.label4);
            this.ChatPanel.Controls.Add(this.buttonAcknowledgeAlarm);
            this.ChatPanel.Controls.Add(this.buttonMaskSensor);
            this.ChatPanel.Controls.Add(this.textBoxUserName);
            this.ChatPanel.Controls.Add(this.buttonChangeUser);
            this.ChatPanel.Controls.Add(this.TextBoxPassword);
            this.ChatPanel.Controls.Add(this.RichTextBoxConsole);
            this.ChatPanel.Controls.Add(this.TextBoxMessage);
            this.ChatPanel.Controls.Add(this.ButtonSend);
            this.ChatPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ChatPanel.Location = new System.Drawing.Point(0, 0);
            this.ChatPanel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChatPanel.Name = "ChatPanel";
            this.ChatPanel.Size = new System.Drawing.Size(752, 789);
            this.ChatPanel.TabIndex = 4;
            this.ChatPanel.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(245, 20);
            this.label4.TabIndex = 9;
            this.label4.Text = "Type a message and press Send:";
            // 
            // buttonAcknowledgeAlarm
            // 
            this.buttonAcknowledgeAlarm.Enabled = false;
            this.buttonAcknowledgeAlarm.Location = new System.Drawing.Point(134, 106);
            this.buttonAcknowledgeAlarm.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonAcknowledgeAlarm.Name = "buttonAcknowledgeAlarm";
            this.buttonAcknowledgeAlarm.Size = new System.Drawing.Size(112, 35);
            this.buttonAcknowledgeAlarm.TabIndex = 8;
            this.buttonAcknowledgeAlarm.Text = "Ack Alarm";
            this.buttonAcknowledgeAlarm.UseVisualStyleBackColor = true;
            this.buttonAcknowledgeAlarm.Visible = false;
            // 
            // buttonMaskSensor
            // 
            this.buttonMaskSensor.Enabled = false;
            this.buttonMaskSensor.Location = new System.Drawing.Point(14, 106);
            this.buttonMaskSensor.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonMaskSensor.Name = "buttonMaskSensor";
            this.buttonMaskSensor.Size = new System.Drawing.Size(112, 35);
            this.buttonMaskSensor.TabIndex = 7;
            this.buttonMaskSensor.Text = "Mask Sensor";
            this.buttonMaskSensor.UseVisualStyleBackColor = true;
            this.buttonMaskSensor.Visible = false;
            // 
            // textBoxUserName
            // 
            this.textBoxUserName.Location = new System.Drawing.Point(284, 106);
            this.textBoxUserName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBoxUserName.Name = "textBoxUserName";
            this.textBoxUserName.Size = new System.Drawing.Size(286, 26);
            this.textBoxUserName.TabIndex = 6;
            this.textBoxUserName.Visible = false;
            // 
            // buttonChangeUser
            // 
            this.buttonChangeUser.Enabled = false;
            this.buttonChangeUser.Location = new System.Drawing.Point(620, 106);
            this.buttonChangeUser.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonChangeUser.Name = "buttonChangeUser";
            this.buttonChangeUser.Size = new System.Drawing.Size(112, 35);
            this.buttonChangeUser.TabIndex = 5;
            this.buttonChangeUser.Text = "User";
            this.buttonChangeUser.UseVisualStyleBackColor = true;
            this.buttonChangeUser.Visible = false;
            this.buttonChangeUser.Click += new System.EventHandler(this.buttonChangeUser_Click);
            // 
            // TextBoxPassword
            // 
            this.TextBoxPassword.Location = new System.Drawing.Point(284, 135);
            this.TextBoxPassword.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.TextBoxPassword.Name = "TextBoxPassword";
            this.TextBoxPassword.Size = new System.Drawing.Size(286, 26);
            this.TextBoxPassword.TabIndex = 4;
            this.TextBoxPassword.Visible = false;
            // 
            // SignInPanel
            // 
            this.SignInPanel.Controls.Add(this.SignInMessagesTextBox);
            this.SignInPanel.Controls.Add(this.DomainTextBox);
            this.SignInPanel.Controls.Add(this.SwitchUserBtn);
            this.SignInPanel.Controls.Add(this.label3);
            this.SignInPanel.Controls.Add(this.URITextBox);
            this.SignInPanel.Controls.Add(this.labelWorkstationName);
            this.SignInPanel.Controls.Add(this.PasswordTextBox);
            this.SignInPanel.Controls.Add(this.label2);
            this.SignInPanel.Controls.Add(this.ConnectBtn);
            this.SignInPanel.Controls.Add(this.UserNameTextBox);
            this.SignInPanel.Controls.Add(this.label1);
            this.SignInPanel.Controls.Add(this.StatusText);
            this.SignInPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SignInPanel.Location = new System.Drawing.Point(0, 0);
            this.SignInPanel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.SignInPanel.Name = "SignInPanel";
            this.SignInPanel.Size = new System.Drawing.Size(752, 789);
            this.SignInPanel.TabIndex = 4;
            // 
            // SignInMessagesTextBox
            // 
            this.SignInMessagesTextBox.Location = new System.Drawing.Point(17, 232);
            this.SignInMessagesTextBox.Multiline = true;
            this.SignInMessagesTextBox.Name = "SignInMessagesTextBox";
            this.SignInMessagesTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.SignInMessagesTextBox.Size = new System.Drawing.Size(709, 545);
            this.SignInMessagesTextBox.TabIndex = 800;
            this.SignInMessagesTextBox.TabStop = false;
            // 
            // DomainTextBox
            // 
            this.DomainTextBox.Location = new System.Drawing.Point(12, 72);
            this.DomainTextBox.Name = "DomainTextBox";
            this.DomainTextBox.Size = new System.Drawing.Size(315, 26);
            this.DomainTextBox.TabIndex = 8;
            // 
            // SwitchUserBtn
            // 
            this.SwitchUserBtn.Location = new System.Drawing.Point(627, 28);
            this.SwitchUserBtn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.SwitchUserBtn.Name = "SwitchUserBtn";
            this.SwitchUserBtn.Size = new System.Drawing.Size(112, 35);
            this.SwitchUserBtn.TabIndex = 7;
            this.SwitchUserBtn.Text = "Switch User";
            this.SwitchUserBtn.UseVisualStyleBackColor = true;
            this.SwitchUserBtn.Click += new System.EventHandler(this.SwitchUserBtn_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(266, 12);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(124, 20);
            this.label3.TabIndex = 11;
            this.label3.Text = "Enter password:";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // URITextBox
            // 
            this.URITextBox.Location = new System.Drawing.Point(17, 169);
            this.URITextBox.Name = "URITextBox";
            this.URITextBox.Size = new System.Drawing.Size(575, 26);
            this.URITextBox.TabIndex = 9;
            this.URITextBox.TextChanged += new System.EventHandler(this.URITextBox_TextChanged);
            // 
            // labelWorkstationName
            // 
            this.labelWorkstationName.AutoSize = true;
            this.labelWorkstationName.Location = new System.Drawing.Point(119, 131);
            this.labelWorkstationName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelWorkstationName.Name = "labelWorkstationName";
            this.labelWorkstationName.Size = new System.Drawing.Size(41, 20);
            this.labelWorkstationName.TabIndex = 99;
            this.labelWorkstationName.Text = "TBD";
            // 
            // PasswordTextBox
            // 
            this.PasswordTextBox.Location = new System.Drawing.Point(261, 37);
            this.PasswordTextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.PasswordTextBox.Name = "PasswordTextBox";
            this.PasswordTextBox.PasswordChar = '*';
            this.PasswordTextBox.Size = new System.Drawing.Size(206, 26);
            this.PasswordTextBox.TabIndex = 5;
            this.PasswordTextBox.Text = "puffin";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 131);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 20);
            this.label2.TabIndex = 799;
            this.label2.Text = "Workstation:";
            // 
            // ConnectBtn
            // 
            this.ConnectBtn.Location = new System.Drawing.Point(627, 165);
            this.ConnectBtn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ConnectBtn.Name = "ConnectBtn";
            this.ConnectBtn.Size = new System.Drawing.Size(112, 35);
            this.ConnectBtn.TabIndex = 10;
            this.ConnectBtn.Text = "Connect";
            this.ConnectBtn.UseVisualStyleBackColor = true;
            this.ConnectBtn.Click += new System.EventHandler(this.SignInButton_Click);
            // 
            // UserNameTextBox
            // 
            this.UserNameTextBox.Location = new System.Drawing.Point(10, 37);
            this.UserNameTextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.UserNameTextBox.Name = "UserNameTextBox";
            this.UserNameTextBox.Size = new System.Drawing.Size(206, 26);
            this.UserNameTextBox.TabIndex = 4;
            this.UserNameTextBox.Text = "ESITest";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 12);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 20);
            this.label1.TabIndex = 99;
            this.label1.Text = "Enter user name:";
            // 
            // StatusText
            // 
            this.StatusText.Location = new System.Drawing.Point(10, 209);
            this.StatusText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.StatusText.Name = "StatusText";
            this.StatusText.Size = new System.Drawing.Size(716, 20);
            this.StatusText.TabIndex = 9;
            this.StatusText.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.StatusText.Visible = false;
            // 
            // WinFormsClient
            // 
            this.AcceptButton = this.ButtonSend;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(752, 789);
            this.Controls.Add(this.SignInPanel);
            this.Controls.Add(this.ChatPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(764, 819);
            this.Name = "WinFormsClient";
            this.Text = "WinForms SignalR Client";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.WebFormsClient_FormClosing);
            this.ChatPanel.ResumeLayout(false);
            this.ChatPanel.PerformLayout();
            this.SignInPanel.ResumeLayout(false);
            this.SignInPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ButtonSend;
        private System.Windows.Forms.TextBox TextBoxMessage;
        private System.Windows.Forms.RichTextBox RichTextBoxConsole;
        private System.Windows.Forms.Panel ChatPanel;
        private System.Windows.Forms.Panel SignInPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button ConnectBtn;
        private System.Windows.Forms.TextBox UserNameTextBox;
        private System.Windows.Forms.Label StatusText;
        private System.Windows.Forms.Button buttonAcknowledgeAlarm;
        private System.Windows.Forms.Button buttonMaskSensor;
        private System.Windows.Forms.TextBox textBoxUserName;
        private System.Windows.Forms.Button buttonChangeUser;
        private System.Windows.Forms.TextBox TextBoxPassword;
        private System.Windows.Forms.Label labelWorkstationName;
        private System.Windows.Forms.TextBox PasswordTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox URITextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button SwitchUserBtn;
        private System.Windows.Forms.TextBox DomainTextBox;
        private System.Windows.Forms.TextBox SignInMessagesTextBox;
        private System.Windows.Forms.Label label4;
    }
}

