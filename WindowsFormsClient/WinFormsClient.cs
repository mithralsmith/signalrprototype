﻿using Microsoft.AspNet.SignalR.Client;
using System;
using System.Diagnostics.Contracts;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices.Protocols;
using System.Drawing;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Contexts;
using System.Security;
using System.Security.Principal;
using System.Windows.Forms;
using ApprovalTests.Namers;

namespace WinFormsClient
{
    /// <summary>
    /// SignalR client hosted in a WinForms application. The client
    /// lets the user pick a user name, connect to the server asynchronously
    /// to not block the UI thread, and send chat messages to all connected 
    /// clients whether they are hosted in WinForms, WPF, or a web application.
    /// </summary>
    public partial class WinFormsClient : Form
    {
        /// <summary>
        /// This name is simply added to sent messages to identify the user; this 
        /// sample does not include authentication.
        /// </summary>
        private String UserName { get; set; }
        private String WorkstationName { get; set; }
        private string Password { get; set; }
        private IHubProxy HubProxy { get; set; }
        protected string ServerURI { get; set; }
        private HubConnection Connection { get; set; }
        
        internal WinFormsClient()
        {
            ServerURI = Properties.Settings.Default.ServerURI;
            InitializeComponent();
            WorkstationName = WindowsIdentity.GetCurrent().Name;
            URITextBox.Text = ServerURI;
            DomainTextBox.Text = Environment.UserDomainName;

        }

        private void ButtonSend_Click(object sender, EventArgs e)
        {
            HubProxy.Invoke("Send", UserName, TextBoxMessage.Text);
            TextBoxMessage.Text = String.Empty;
            TextBoxMessage.Focus();
        }

        /// <summary>
        /// Creates and connects the hub connection and hub proxy. This method
        /// is called asynchronously from SignInButton_Click.
        /// </summary>
        private async void ConnectAsync()
        {
            Connection = new HubConnection(ServerURI);
            Connection.Credentials = CredentialCache.DefaultCredentials;
            Connection.Closed += Connection_Closed;
            Connection.Error += Connection_Error;
            Connection.ConnectionSlow += Connection_ConnectionSlow;
            Connection.Reconnecting += Connection_Reconnecting;
            Connection.Reconnected += Connection_Reconnected;

            HubProxy = Connection.CreateHubProxy("MyHub");
            //Handle incoming event from server: use Invoke to write to console from SignalR's thread
            HubProxy.On<string, string>("AddMessage", (name, message) =>
                this.Invoke((Action)(() =>
                    RichTextBoxConsole.AppendText(String.Format("{0}: {1}" + Environment.NewLine, name, message))
                ))
            );
            try
            {
                await Connection.Start();
            }
            catch (HttpRequestException ex)
            {
                StatusText.Text = "Unable to connect to server: Start server before connecting clients.";
                //No connection: Don't enable Send button or show chat UI
                DisplaySignInException(ex);
                Exception innException = ex.InnerException;
                while (innException != null)
                {
                    DisplaySignInException(innException);
                    innException = innException.InnerException;
                }
                return;
            }

            //Activate UI
            SignInPanel.Visible = false;
            ChatPanel.Visible = true;
            ButtonSend.Enabled = true;
            TextBoxMessage.Focus();
            RichTextBoxConsole.AppendText("Connected to server at " + ServerURI + Environment.NewLine);
  
        }
        private void DisplaySignInException(Exception ex)
        {
            DisplaySignInMessage($"Error Occurred in Client: {ex.Message}{Environment.NewLine}");
        }

        private void DisplaySignInMessage(string message)
        {
            if (InvokeRequired)
            {
                Invoke((Action)(() =>
                {
                    DisplaySignInMessage(message);
                }));
            }
            SignInMessagesTextBox.Text =
                $@"{message}{Environment.NewLine}{SignInMessagesTextBox.Text}";
        }
        private void DisplayException(Exception ex)
        {
            Invoke((Action)(() =>
            {
                RichTextBoxConsole.ForeColor = Color.Red;
                RichTextBoxConsole.AppendText($"Error Occurred in Server: {ex.Message}\n");
                RichTextBoxConsole.ForeColor = Color.Black;
                RichTextBoxConsole.SelectionIndent = 10;
                RichTextBoxConsole.AppendText($"{ex}\n");
                RichTextBoxConsole.SelectionIndent = 0;
                RichTextBoxConsole.ScrollToCaret();
            }));
        }


        private void Connection_Reconnected()
        {
            Invoke((Action) (() =>
            {
                RichTextBoxConsole.ForeColor = Color.Green;
                RichTextBoxConsole.AppendText($"Reconnected.\n");
                RichTextBoxConsole.ForeColor = Color.Black;
                RichTextBoxConsole.ScrollToCaret();
            }));
        }

        private void Connection_Reconnecting()
        {
            Invoke((Action) (() =>
            {
                RichTextBoxConsole.ForeColor = Color.DarkOrange;
                RichTextBoxConsole.AppendText($"Reconnecting.\n");
                RichTextBoxConsole.ForeColor = Color.Black;
                RichTextBoxConsole.ScrollToCaret();
            }));
        }

        private void Connection_ConnectionSlow()
        {
            Invoke((Action) (() =>
            {
                RichTextBoxConsole.ForeColor = Color.Blue;
                RichTextBoxConsole.AppendText($"Slow connection.\n");
                RichTextBoxConsole.ForeColor = Color.Black;
                RichTextBoxConsole.ScrollToCaret();
            }));
        }

        private void Connection_Error(Exception ex)
        {
            DisplayException(ex);
        }

        /// <summary>
        /// If the server is stopped, the connection will time out after 30 seconds (default), and the 
        /// Closed event will fire.
        /// </summary>
        private void Connection_Closed()
        {
            //Deactivate chat UI; show login UI.
            Invoke((Action) (() =>
            {
                ChatPanel.Visible = false;
                ButtonSend.Enabled = false;
                StatusText.Text = "You have been disconnected.";
                SignInPanel.Visible = true;
            }));
        }

        private void SignInButton_Click(object sender, EventArgs e)
        {
            UserName = UserNameTextBox.Text;
            //Connect to server (use async method to avoid blocking UI thread)
            if (!String.IsNullOrEmpty(UserName))
            {
                StatusText.Visible = true;
                StatusText.Text = "Connecting to server...";
                ConnectAsync();
            }
        }

        private void WebFormsClient_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Connection != null)
            {
                Connection.Stop();
                Connection.Dispose();
            }
        }

        private void buttonChangeUser_Click(object sender, EventArgs e)
        {

        }

        private void URITextBox_TextChanged(object sender, EventArgs e)
        {
            ServerURI = URITextBox.Text;
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void SwitchUserBtn_Click(object sender, EventArgs e)
        {
            var username = $"{UserNameTextBox.Text}";
            if (!string.IsNullOrEmpty(DomainTextBox.Text))

            {
                username = $"{DomainTextBox.Text}/{username}";
            }
            DisplaySignInMessage($"Sign in as {username}\n");
            var password = PasswordTextBox.Text;
            try
            {

                var identity = GetIdentity(username, password);
            }
            catch (Exception ex)
            {
                DisplaySignInException(ex);
            }

            var authenticated = IsAuthenticated(DomainTextBox.Text, UserNameTextBox.Text, password);



            var valid = false;
            try
            {
                //NOTE this technique only works with the local domain, the username cannot
                // be prefixed with the domain name. DomainName/Username  It must be just username.
                using (PrincipalContext context = new PrincipalContext(ContextType.Domain))
                {
                    valid = context.ValidateCredentials(username, password);
                }
            }
            catch (Exception ex)
            {
                DisplaySignInMessage(
                    $"Error authenticating with ContextType.Domain: {ex.Message}{Environment.NewLine}");

                try
                {
                    using (PrincipalContext context = new PrincipalContext(ContextType.ApplicationDirectory))
                    {
                        valid = context.ValidateCredentials(username, password);
                    }
                }
                catch (Exception ex2)
                {
                    DisplaySignInMessage(
                        $"Error authenticating with ContextType.ApplicationDirectory: {ex2.Message}{Environment.NewLine}");
                    try
                    {
                        using (PrincipalContext context = new PrincipalContext(ContextType.Machine))
                        {
                            valid = context.ValidateCredentials(username, password);
                        }
                    }
                    catch (Exception ex3)
                    {
                        DisplaySignInMessage(
                            $"Error authenticating with ContextType.Machine: {ex3.Message}{Environment.NewLine}");
                    }
                }
            }

            if (!valid)
            {

            }
            if (valid)
            {
                DisplaySignInMessage(
                            $"User {username} is valid.{Environment.NewLine}");
                ConnectBtn.Enabled = true;
            }
            //TODO impersonate user
            //TODO add serialized token
            //ToDO verify we can pass the serialized token
            //http://stackoverflow.com/questions/15251906/how-to-store-windowsidentity-for-use-at-a-later-indeterminate-time
        }

        public bool IsAuthenticated(string domain, string username, string password)
        {
            bool authenticated = false;

            try
            {
                DirectoryEntry entry = new DirectoryEntry("LDAP://"+domain, username, password);
                object nativeObject = entry.NativeObject;
                authenticated = true;
            }
            catch (DirectoryServicesCOMException cex)
            {
                SignInMessagesTextBox.Text = $"Error: {cex.Message}{Environment.NewLine} {SignInMessagesTextBox.Text}";
                SignInMessagesTextBox.ScrollToCaret();
            }
            catch (Exception ex)
            {
                SignInMessagesTextBox.Text = $"Error: {ex.Message}{Environment.NewLine} {SignInMessagesTextBox.Text}";
                SignInMessagesTextBox.ScrollToCaret();
            }

            return authenticated;
        }

        //LDAP validation
        public bool fnValidateUser(string username, string password, string domain = null)
        {
            bool validation;
            domain = domain ?? Environment.UserDomainName;
            try
            {
                LdapConnection lcon = new LdapConnection
                        (new LdapDirectoryIdentifier((string)null, false, false));
                NetworkCredential nc = new NetworkCredential(username,
                                       password, domain);
                lcon.Credential = nc;
                lcon.AuthType = AuthType.Negotiate;
                // user has authenticated at this point,
                // as the credentials were used to login to the dc.
                lcon.Bind(nc);
                validation = true;
            }
            catch (LdapException ex)
            {
                SignInMessagesTextBox.Text = $"Error: {ex.Message}{Environment.NewLine} {SignInMessagesTextBox.Text}";
                SignInMessagesTextBox.ScrollToCaret();
                validation = false;
            }
            return validation;
        }

        [DllImport("advapi32.dll", SetLastError = true)]
        public static extern bool LogonUser(
        string lpszUsername,
        string lpszDomain,
        string lpszPassword,
        int dwLogonType,
        int dwLogonProvider,
        out IntPtr phToken);

        public WindowsIdentity GetIdentity(string username, string password)
        {
            var userToken = IntPtr.Zero;

            var success = LogonUser(
              "username",
              "domain",
              "password",
              2, // LOGON32_LOGON_INTERACTIVE
              0, // LOGON32_PROVIDER_DEFAULT
              out userToken);

            if (!success)
            {
                throw new SecurityException($"User logon failed: { Marshal.GetLastWin32Error()}");
            }

            var identity = new WindowsIdentity(userToken);

            return identity;
        }
    }
}
