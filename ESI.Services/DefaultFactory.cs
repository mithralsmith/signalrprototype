﻿using System.Net;
using ESI.Interfaces;

namespace ESI.Services
{
    public class DefaultFactory
    {
        public static IAuthorizationService AuthorizationService = new FakeAuthorizationService();
        public static IPermissionsService PermissionService = new FakePermissionService();

    }
}