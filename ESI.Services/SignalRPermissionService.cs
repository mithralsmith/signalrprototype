﻿using System.Collections;
using System.Collections.Generic;
using ESI.Interfaces;

namespace ESI.Services
{
    public class FakePermissionService:IPermissionsService
    {
        private static List<IPermission> _workstationPermissions = new List<IPermission>()
        {
            new Permission(PermissionAction.StartAlertModule),
        };

        private static List<IPermission> _operatorPermissions = new List<IPermission>()
        {
            new Permission(PermissionAction.ReceiveAlerts),
            new Permission(PermissionAction.Logoff),

        };
        #region Implementation of IPermissionsService

        public void PopulatePermissions(IPopulatePermissions receiver)
        {
            if (receiver is IWorkstationContext)
            {
                receiver.PopulatePermissions(_workstationPermissions);
            }
            else if (receiver is IUserContext)
            {
                receiver.PopulatePermissions(_operatorPermissions);
            }
            else
            {
                //has no permissions
                receiver.PopulatePermissions(new List<IPermission>());
            }
        }

        #endregion
    }
}