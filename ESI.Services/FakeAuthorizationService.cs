﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESI.Interfaces;

namespace ESI.Services
{
    public class FakeAuthorizationService: IAuthorizationService
    {
        #region Implementation of IAuthorizationService

        /// <summary>
        /// Verifies that the context is authorized and sets the properites
        /// accordingly.
        /// </summary>
        /// <param name="context">The user context to authorize</param>
        /// <returns>Returns true if the context is authorized for this application.</returns>
        /// <remarks>As a side effect the context is populated with the appropriate permissions.</remarks>
        public bool Authorize(IUserContext context)
        {
            IPopulatePermissions pp = context as IPopulatePermissions;
            if (pp != null)
            {
                var permisisonService = new FakePermissionService();
                permisisonService.PopulatePermissions(pp);
                return context.PermissionList.Any();
            }
            else
            {
                return false;
            }
        }

        #endregion
    }
}
