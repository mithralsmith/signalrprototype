﻿using System.Collections.Generic;

namespace testInterfaces
{
    public class BaseType : IBaseType
    {
        private string Property1 { get; set; }
        public int PProperty2 { get; set; }

        public IList<string> StringList
        {
            get { return _StringList; }
            set { _StringList = value; }
        }

        private IList<string> _StringList;

        public BaseType()
        {
            
        }

    }

}