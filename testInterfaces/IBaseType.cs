﻿using System.Collections.Generic;

namespace testInterfaces
{
    public interface IBaseType
    {
        int PProperty2 { get; set; }
        IList<string> StringList { get; set; }
    }
}