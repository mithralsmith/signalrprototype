﻿using System.Collections.Generic;

namespace testInterfaces
{
    public interface IChildType:IBaseType
    {
        string BackingField { get; set; }
        IEnumerable<IBaseType> EnumerableStuff { get; set; }
    }
}