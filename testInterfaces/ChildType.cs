﻿using System.Collections.Generic;
using System.Net.Http;

namespace testInterfaces
{
    public class ChildType: BaseType, IChildType
    {
        public int Count
        {
            get { return StringList.Count; }
        }

        public string BackingField
        {
            get { return _BackingField; }
            set { _BackingField = value; }
        }

        public string _NewField;

        private int _NewPrivateField;

        private string _BackingField;

        public IEnumerable<IBaseType> EnumerableStuff { get; set; }

    }
}