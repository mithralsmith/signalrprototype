using System.Threading.Tasks;
using System.Windows;
using Microsoft.AspNet.SignalR;

namespace WPFServer
{
    public class TestHub : Hub
    {
        public void Send(string name, string message)
        {
            Clients.All.addMessage(name, message);
        }
    }
}