﻿using System;
using System.Linq;
using ESI.Context;
using ESI.Services;
using Xunit;

namespace ESI.XUnit.Tests
{
    public class FakePermissionServiceTests
    {
        [Fact]
        public void AuthorizedWorksWithDefaultWorkstationContext()
        {
            var sut = new FakePermissionService();
            //it would better to use a stub/mock instead of a real 
            //  workstation because this now tests both the workstation
            //  and the service.
            var workstationContext = new WorkstationContext();
            sut.PopulatePermissions(workstationContext);
            var expectedPermissions = 1;
            Assert.Equal(expectedPermissions, workstationContext.PermissionList.Count());
        }
    }
}
