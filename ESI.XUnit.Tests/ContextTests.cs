﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using ESI.Context;
using ESI.Interfaces;
using ESI.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ESI.XUnit.Tests
{
    /// <summary>
    /// Summary description for ContextTests
    /// </summary>
    [TestClass]
    public class ContextTests
    {
        public ContextTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void EmptyConstructor_IsAuthenticated()
        {
            var sut = new WorkstationContext();
            //is authenticated because the current user is authenticated

            Assert.IsTrue(sut.IsAuthenticated);
        }
        [TestMethod]
        public void EmptyConstructor_IsAuthorized()
        {
            var sut = new WorkstationContext();
           
            Assert.IsTrue(sut.IsAuthorized);
        }
        [TestMethod]
        public void EmptyConstructor_NameIsLoggedInUser()
        {
            var sut = new WorkstationContext();
            var expected = WindowsIdentity.GetCurrent().Name;
            Assert.AreEqual(expected, sut.Name);
        }

        [TestMethod]
        public void EmptyConstructor_Authorize_ProducesTrueIsAuthenticated()
        {
            var sut = new WorkstationContext();
            sut.Authorize();
            //is authenticated because the current user is authenticated

            Assert.IsTrue(sut.IsAuthenticated);
        }
        [TestMethod]
        public void EmptyConstructor_Authorize_ProducesPermissionList()
        {
            var sut = new WorkstationContext();
            sut.Authorize();
            //is authenticated because the current user is authenticated
            var expected = 1;
           
            Assert.AreEqual(expected, sut.PermissionList.Count());
        }
        [TestMethod]
        public void EmptyConstructor_Authorize_ProducesTrueIsAuthorizedAlerts()
        {
            IUserContext sut = new WorkstationContext();
            sut.Authorize();

            Assert.IsTrue(((IWorkstationContext)sut).IsAuthorizedAlerts);
        }

        [TestMethod]
        public void EmptyConstructor_AuthorizeTwice_ProducesTrueIsAuthenticated()
        {
            var sut = new WorkstationContext();
            sut.Authorize();
            sut.Authorize();
            //is authenticated because the current user is authenticated

            Assert.IsTrue(sut.IsAuthenticated);
        }
    }
}
