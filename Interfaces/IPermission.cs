﻿namespace ESI.Interfaces
{
    public interface IPermission
    {
        /// <summary>
        /// The name associated with this permission.
        /// </summary>
        string Name { get; }
        /// <summary>
        /// The action associated with this permission
        /// </summary>
        PermissionAction Action { get; }
        //TODO consider multiple actions for a given permission
        // to represent different actions which are allowed if the user
        // has this permission.   For now a one-to-one relationship is all
        // that is allowed.
    }
}