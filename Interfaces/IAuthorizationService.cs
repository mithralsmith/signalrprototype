﻿

namespace ESI.Interfaces
{
    public interface IAuthorizationService
    {


        /// <summary>
        /// Verifies that the context is authorized and sets the properites
        /// accordingly.
        /// </summary>
        /// <param name="context">The user context to authorize</param>
        /// <returns>Returns true if the context is authorized for this application.</returns>
        /// <remarks>As a side effect the context is populated with the appropriate permissions.</remarks>
        bool Authorize(IUserContext context);
    }
}