﻿namespace ESI.Interfaces
{
    public interface IPermissionsService
    {
        void PopulatePermissions(IPopulatePermissions receiver);
    }
}