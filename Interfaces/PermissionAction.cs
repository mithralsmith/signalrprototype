﻿namespace ESI.Interfaces
{
    public enum PermissionAction
    {
        StartAlertModule,
        ReceiveAlerts,
        Logoff
    }
}