﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESI.Interfaces
{
    public class Permission: IPermission
    {
        private string _name;
        private PermissionAction _action;

        public Permission(PermissionAction action)
        {
            _name = action.ToString();
            _action = action;

        }
        public Permission(string name, PermissionAction action)
        {
            _name = name;
            _action = action;
        }

        #region Implementation of IPermission

        /// <summary>
        /// The name associated with this permission.
        /// </summary>
        public string Name
        {
            get { return _name; }
        }

        /// <summary>
        /// The action associated with this permission
        /// </summary>
        public PermissionAction Action
        {
            get { return _action; }
        }

        #endregion
    }
}
