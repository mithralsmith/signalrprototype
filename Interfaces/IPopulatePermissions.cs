﻿using System.Collections.Generic;

namespace ESI.Interfaces
{
    public interface IPopulatePermissions
    {
        void PopulatePermissions(IEnumerable<IPermission> permissionList);
    }


}