﻿using System.Collections.Generic;
using System.Dynamic;

namespace ESI.Interfaces
{
    public interface IUserContext
    {
        /// <summary>
        /// Get only the name of the workstation.
        /// </summary>
        string Name { get; }
        /// <summary>
        /// Get only is this workstation authenticated. 
        /// </summary>
        bool IsAuthenticated { get; }
        /// <summary>
        /// Get only is this workstation authorized to use any modules.
        /// </summary>
        bool IsAuthorized { get; }

        /// <summary>
        /// Determines if the user has the specified permission;
        /// </summary>
        /// <param name="action">The name of the permission.</param>
        /// <returns>True if the user in this context has the specified permission.</returns>
        bool HasPermission(PermissionAction action);
        
        /// <summary>
        /// get only a list of permissions;
        /// </summary>
        IEnumerable<IPermission> PermissionList { get; }

        /// <summary>
        /// Verifies that the context is authorized and sets the available permissions
        /// accordingly.
        /// </summary>
        /// <returns>Returns true if the context is authorized for this application.</returns>
        bool Authorize();
    }
}