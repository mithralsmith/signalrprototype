﻿namespace ESI.Interfaces
{
    public interface IWorkstationContext:IUserContext
    {
        /// <summary>
        /// Get only is this workstation authorized to use the Alerts module.
        /// </summary>
        bool IsAuthorizedAlerts { get; }

    }
}